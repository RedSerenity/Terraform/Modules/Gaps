
/* Docker Module Passthrough */
variable "name" {
	type = string
}

variable "docker_network" {
	type = string
}

variable "docker_data" {
	type = string
}

variable "stack" {
	type = string
}

variable "internal_domain_base" {
	type = string
	default = "flix"
}