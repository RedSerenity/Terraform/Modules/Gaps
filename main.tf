data "docker_registry_image" "Gaps" {
	name = "housewrecker/gaps:latest"
}

resource "docker_image" "Gaps" {
	name = data.docker_registry_image.Gaps.name
	pull_triggers = [data.docker_registry_image.Gaps.sha256_digest]
}

module "Gaps" {
  source = "gitlab.com/RedSerenity/docker/local"

	name = var.name
	image = docker_image.Gaps.latest

	networks = [{ name: var.docker_network, aliases: ["gaps.${var.internal_domain_base}"] }]
  ports = [{ internal: 8484, external: 9607, protocol: "tcp" }]
	volumes = [
		{
			host_path = "${var.docker_data}/Gaps"
			container_path = "/config"
			read_only = false
		}
	]

	environment = {
		"BASE_URL": "/gaps"
	}

	stack = var.stack
}